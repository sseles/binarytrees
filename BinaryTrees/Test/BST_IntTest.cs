﻿namespace Test
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using BinaryTrees.Samples.BSTs;
    using BinaryTrees.Samples;

    [TestClass]
    public class BST_IntTest
    {
        [TestMethod]
        public void Add()
        {
            BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction)
            {
                5,
                15,
                25,
                13,
                2,
                19,
                23,
                1
            };
            bool result =
                tree.Root.Get.Value == 5 &&
                tree.Root.Left.Get.Value == 2 &&
                tree.Root.Left.Left.Get.Value == 1 &&
                tree.Root.Right.Get.Value == 15 &&
                tree.Root.Right.Left.Get.Value == 13 &&
                tree.Root.Right.Right.Get.Value == 25 &&
                tree.Root.Right.Right.Left.Get.Value == 19 &&
                tree.Root.Right.Right.Left.Right.Get.Value == 23 &&
                tree.Root.Right.Right.Parent.Equals(tree.Root.Right)
            ;

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Has()
        {
            BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction)
            {
                5,
                15,
                25,
                13,
                2,
                19,
                23,
                1
            };
            bool result = tree.Contains(5) && tree.Contains(2) && tree.Contains(23);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Search()
        {
            BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction)
            {
                5,
                15,
                25,
                13,
                2,
                19,
                23,
                1
            };
            var _5 = tree.Find(5);
            var _2 = tree.Find(2);
            var _23 = tree.Find(23);
            var _45 = tree.Find(45);

            bool result = _5 == 5 && _2 == 2 && _23 == 23 && _45 == 0;

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RemoveRoot()
        {
            BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction)
            {
                5
            };
            tree.Remove(5);

            var result = tree.Contains(5);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void RemoveChildWithLeftNull()
        {
            BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction)
            {
                5,
                15,
                25,
                35
            };
            tree.Remove(15);

            var result = !tree.Contains(15) && tree.Contains(25) && tree.Contains(35);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RemoveChildWithRightNull()
        {
            BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction)
            {
                5,
                4,
                3,
                1
            };
            tree.Remove(4);

            var result = !tree.Contains(4) && tree.Contains(3) && tree.Contains(1);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RemoveLeft()
        {
            BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction)
            {
                5,
                4,
                2,
                3,
                1
            };
            tree.Remove(4);

            var result = !tree.Contains(4) && tree.Contains(3) && tree.Contains(2) && tree.Contains(1);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RemoveComplex()
        {
            BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction)
            {
                8,
                4,
                2,
                6,
                3,
                5,
                7,
                12,
                10,
                14,
                11,
                13,
                16,
                1
            };
            tree.Remove(8);

            var result = !tree.Contains(8) && tree.Contains(3) && tree.Contains(2) && tree.Contains(11) && !tree.Contains(9) && tree.Contains(13) && tree.Contains(16);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RemoveComplex2()
        {
            BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction)
            {
                8,
                4,
                2,
                6,
                3,
                5,
                7,
                1
            };
            tree.Remove(8);

            var a = !tree.Contains(8);
            var b = tree.Contains(4);
            var c = tree.Contains(2);
            var d = tree.Contains(6);
            var e = tree.Contains(3);
            var f = tree.Contains(5);
            var g = tree.Contains(7);
            var h = tree.Contains(1);

            var result = a && b && c && d && e && f && g && h;

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RemoveComplex3()
        {
            BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction)
            {
                8,
                4,
                2,
                6,
                3,
                5,
                1
            };
            tree.Remove(4);

            var a = tree.Contains(8);
            var b = !tree.Contains(4);
            var c = tree.Contains(2);
            var d = tree.Contains(6);
            var e = tree.Contains(3);
            var f = tree.Contains(5);
            var h = tree.Contains(1);

            var result = a && b && c && d && e && f && h;

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RemoveComplex4()
        {
            BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction)
            {
                8,
                4,
                2,
                6,
                3,
                5,
                1
            };
            tree.Remove(6);

            var a = tree.Contains(8);
            var b = tree.Contains(4);
            var c = tree.Contains(2);
            var d = !tree.Contains(6);
            var e = tree.Contains(3);
            var f = tree.Contains(5);
            var h = tree.Contains(1);

            var result = a && b && c && d && e && f && h;

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RemoveComplex5()
        {
            BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction)
            {
                8,
                4,
                2,
                6,
                3,
                5,
                1
            };
            tree.Remove(5);

            var a = tree.Contains(8);
            var b = tree.Contains(4);
            var c = tree.Contains(2);
            var d = tree.Contains(6);
            var e = tree.Contains(3);
            var f = !tree.Contains(5);
            var h = tree.Contains(1);

            var result = a && b && c && d && e && f && h;

            Assert.IsTrue(result);
        }


        [TestMethod]
        public void RemoveComplex6()
        {
            BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction)
            {
                8,
                4,
                2,
                6,
                3,
                5,
                7,
                12,
                10,
                14,
                11,
                13,
                16,
                1
            };
            tree.Remove(4);

            var a = tree.Contains(8);
            var b = !tree.Contains(4);
            var c = tree.Contains(2);
            var d = tree.Contains(6);
            var e = tree.Contains(3);
            var f = tree.Contains(5);
            var g = tree.Contains(7);
            var h = tree.Contains(12);
            var i = tree.Contains(10);
            var j = tree.Contains(14);
            var k = tree.Contains(11);
            var l = tree.Contains(13);
            var m = tree.Contains(16);

            var result = a && b && c && d && e && f && g && h && i && j && k && l && m;

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RemoveComplex7()
        {
            BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction)
            {
                8,
                4,
                2,
                6,
                3,
                5,
                7,
                12,
                10,
                14,
                11,
                13,
                16,
                1
            };
            tree.Remove(5);

            var a = tree.Contains(8);
            var b = tree.Contains(4);
            var c = tree.Contains(2);
            var d = tree.Contains(6);
            var e = tree.Contains(3);
            var f = !tree.Contains(5);
            var g = tree.Contains(7);
            var h = tree.Contains(12);
            var i = tree.Contains(10);
            var j = tree.Contains(14);
            var k = tree.Contains(11);
            var l = tree.Contains(13);
            var m = tree.Contains(16);

            var result = a && b && c && d && e && f && g && h && i && j && k && l && m;

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RemoveComplex8()
        {
            BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction)
            {
                17,
                //
                9,
                25,
                //
                4,
                13,
                21,
                29,
                //
                2,
                6,
                11,
                15,
                19,
                23,
                27,
                31,
                //bot
                1,
                3,
                5,
                7,
                10,
                12,
                14,
                16,
                18,
                20,
                22,
                24,
                26,
                28,
                30,
                32,
                8
            };

            tree.Remove(17);
            tree.Remove(12);
            tree.Remove(9);
            tree.Remove(11);
            tree.Remove(13);


            var result = true;
            var rs = "";

            var rs2 = "";

            foreach (var item in tree)
            {
                rs += item + " ";
            }
            for (int i = 1;i<33;i++)
            {
                if (i == 9) 
                    result = result && !tree.Contains(9);
                else if (i == 11) 
                    result = result && !tree.Contains(11);
                else if (i == 12)
                    result = result && !tree.Contains(12);
                else if (i == 13) 
                    result = result && !tree.Contains(13);
                else if (i == 17) 
                    result = result && !tree.Contains(17);
                else
                {
                    result = result & tree.Contains(i);
                    rs2 += i + " ";
                }
                    

            }
            Assert.AreEqual(rs, rs2);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Enumerator()
        {
            BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction)
            {
                8,
                4,
                2,
                6,
                1,
                3,
                5,
                7,
                12,
                10,
                14,
                9,
                11,
                13,
                15
            };
            tree.Remove(5);
            tree.Remove(13);
            tree.Remove(12);

            string items = "";

            foreach (var item in tree)
            {
                items += item + ";";
            }


            Assert.IsTrue(true);
        }

        [TestMethod]
        public void HeavySearch()
        {
            try
            {
                BHT_Int<int> tree = new BHT_Int<int>(StaticFunctions.DefaultFunction);

                for (int i = 0; i < 5000; i++)
                {
                    tree.Add(i);
                }

                var item = tree.Find(4999);

                var result = item == 4999;

                Assert.IsTrue(result);
            }
            catch(Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
}
