﻿namespace Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using BinaryTrees.Parts.Nodes;
    using BinaryTrees.Parts.Pairs;
    using BinaryTrees.Samples;
    using System;

    [TestClass]
    public class NodeTest
    {
        [TestMethod]
        public void NodeWithHash()
        {
            HashValuePair<int, int> input = new HashValuePair<int, int>(5, StaticFunctions.DefaultFunction);
            Node<int, int> node = new Node<int, int>(input);

            Assert.IsTrue(node.Get.Key == 5 && node.Get.Value == 5);
        }

        [TestMethod]
        public void NodeWithId()
        {
            KeyValuePair<int, string> input = new KeyValuePair<int, string>(5, "test");
            Node<int, string> node = new Node<int, string>(input);

            Assert.IsTrue(node.Get.Key == 5 && node.Get.Value == "test");
        }
    }
}
