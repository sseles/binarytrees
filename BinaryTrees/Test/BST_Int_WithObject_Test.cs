﻿namespace Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using BinaryTrees.Samples.BSTs;
    using BinaryTrees.Samples;

    [TestClass]
    public class BST_Int_WithObject_Test
    {
        [TestMethod]
        public void Add()
        {
            BHT_Int<string> tree = new BHT_Int<string>(StaticFunctions.DefaultFunction);

            var _5 = "5".GetHashCode();
            var _15 = "15".GetHashCode();
            var _25 = "25".GetHashCode();
            var _13 = "13".GetHashCode();
            var _2 = "2".GetHashCode();
            var _19 = "19".GetHashCode();
            var _23 = "23".GetHashCode();
            var _1 = "1".GetHashCode();


            tree.Add("5");
            tree.Add("15");
            tree.Add("25");
            tree.Add("13");
            tree.Add("2");
            tree.Add("19");
            tree.Add("23");
            tree.Add("1");


            var a = tree.Root.Get.Value == "5";
            var b = tree.Root.Right.Get.Value == "2";
            var c = tree.Root.Right.Right.Get.Value == "1";
            var d = tree.Root.Left.Get.Value == "15";
            var e = tree.Root.Left.Left.Get.Value == "25";
            var f = tree.Root.Left.Right.Get.Value == "13";
            var g = tree.Root.Left.Right.Right.Get.Value == "19";

            Assert.IsTrue(a && b && c && d && e && f && g);
        }

        [TestMethod]
        public void Has()
        {
            BHT_Int<string> tree = new BHT_Int<string>(StaticFunctions.DefaultFunction)
            {
                "5",
                "15",
                "25",
                "13",
                "2",
                "19",
                "23",
                "1"
            };
            bool result = tree.Contains("5".GetHashCode()) && tree.Contains("2".GetHashCode()) && tree.Contains("23".GetHashCode());

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Search()
        {
            BHT_Int<string> tree = new BHT_Int<string>(StaticFunctions.DefaultFunction)
            {
                "5",
                "15",
                "25",
                "13",
                "2",
                "19",
                "23",
                "1"
            };
            var _5 = tree.Find("5".GetHashCode());
            var _2 = tree.Find("2".GetHashCode());
            var _23 = tree.Find("23".GetHashCode());
            var _45 = tree.Find("45".GetHashCode());



            bool result = _5 == "5" && _2 == "2" && _23 == "23" && _45 == null;

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RemoveRoot()
        {
            BHT_Int<string> tree = new BHT_Int<string>(StaticFunctions.DefaultFunction)
            {
                "5"
            };
            tree.Remove("5".GetHashCode());

            var result = tree.Contains("5".GetHashCode());

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void RemoveChildWithLeftNull()
        {
            BHT_Int<string> tree = new BHT_Int<string>(StaticFunctions.DefaultFunction)
            {
                "5",
                "15",
                "25",
                "35"
            };
            tree.Remove("15".GetHashCode());

            var result = !tree.Contains("15".GetHashCode()) && tree.Contains("25".GetHashCode()) && tree.Contains("35".GetHashCode());

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RemoveChildWithRightNull()
        {
            BHT_Int<string> tree = new BHT_Int<string>(StaticFunctions.DefaultFunction)
            {
                "5",
                "4",
                "3",
                "1"
            };
            tree.Remove("4".GetHashCode());

            var result = !tree.Contains("4".GetHashCode()) && tree.Contains("3".GetHashCode()) && tree.Contains("1".GetHashCode());

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RemoveLeft()
        {
            BHT_Int<string> tree = new BHT_Int<string>(StaticFunctions.DefaultFunction)
            {
                "5",
                "4",
                "3",
                "2",
                "1"
            };
            tree.Remove("4".GetHashCode());

            var result = !tree.Contains("4".GetHashCode()) && tree.Contains("3".GetHashCode()) && tree.Contains("2".GetHashCode()) && tree.Contains("1".GetHashCode());

            Assert.IsTrue(result);
        }


        [TestMethod]
        public void RemoveComplex()
        {
            BHT_Int<string> tree = new BHT_Int<string>(StaticFunctions.DefaultFunction)
            {
                "8",
                "4",
                "2",
                "6",
                "3",
                "5",
                "7",
                "12",
                "10",
                "14",
                "11",
                "13",
                "16",
                "1"
            };
            tree.Remove("8".GetHashCode());

            var result = !tree.Contains("8".GetHashCode()) && tree.Contains("3".GetHashCode()) && tree.Contains("2".GetHashCode()) && tree.Contains("11".GetHashCode()) &&
                         !tree.Contains("9".GetHashCode()) && tree.Contains("13".GetHashCode()) && tree.Contains("16".GetHashCode()) && tree.Contains("5".GetHashCode());

            var bb = "";

            foreach (var a in tree)
            {
                bb += a + "\n";
            }

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestComplex_1()
        {
            var a = new ComplexObject { Id = 0, Second = 0, MyProperty = "0" };
            var b = new ComplexObject { Id = 1, Second = 1, MyProperty = "1" };
            var c = new ComplexObject { Id = 2, Second = 2, MyProperty = "2" };
            var d = new ComplexObject { Id = 3, Second = 3, MyProperty = "3" };
            var e = new ComplexObject { Id = 4, Second = 4, MyProperty = "4" };
            var f = new ComplexObject { Id = 5, Second = 5, MyProperty = "5" };


            BHT_Int<ComplexObject> tree = new BHT_Int<ComplexObject>(StaticFunctions.DefaultFunction)
            {
                a,
                b,
                c,
                d,
                e,
                f
            };


            //tree.First

            //tree.Root.Get.SetKey = 2;

            bool result = true;

            var itemChanged = tree.Find(d.GetHashCode());

            itemChanged.MyProperty = "lavrad";
            itemChanged = new ComplexObject();

            foreach (var item in tree)
            {
                result &= tree.Contains(item.GetHashCode());
            }


            Assert.IsTrue(result);
        }
    }
}
