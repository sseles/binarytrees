﻿namespace Test
{
    using BinaryTrees;
    using BinaryTrees.Parts.Nodes;
    using BinaryTrees.Parts.Pairs;
    using BinaryTrees.Samples;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class BinaryTreeTest
    {
        [TestMethod]
        public void WithHash()
        {
            BinaryTree<int,int> tree = new BinaryTree<int, int>();

            HashValuePair<int, int> input = new HashValuePair<int, int>(5, StaticFunctions.DefaultFunction);
            Node<int, int> node = new Node<int, int>(input);

            tree.Root = node;

            Assert.IsTrue(tree.Root.Get.Key == 5 && tree.Root.Get.Value == 5);
        }

        [TestMethod]
        public void WithoutHash()
        {
            BinaryTree<int, string> tree = new BinaryTree<int, string>();

            KeyValuePair<int, string> input = new KeyValuePair<int, string>(5, "test");
            Node<int, string> node = new Node<int, string>(input);

            tree.Root = node;

            Assert.IsTrue(tree.Root.Get.Key == 5 && tree.Root.Get.Value == "test");
        }
    }
}
