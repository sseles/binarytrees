﻿namespace Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using BinaryTrees.Samples.BSTs;
    using BinaryTrees.Samples;

    [TestClass]
    public class BST_IntKey_WithObject_Test
    {

        [TestMethod]
        public void TestComplex_ChengeKeyOfObject()
        {
            var a = new ComplexObject { Id = 0, Second = 0, MyProperty = "0" };
            var b = new ComplexObject { Id = 1, Second = 1, MyProperty = "1" };
            var c = new ComplexObject { Id = 2, Second = 2, MyProperty = "2" };
            var d = new ComplexObject { Id = 3, Second = 3, MyProperty = "3" };
            var e = new ComplexObject { Id = 4, Second = 4, MyProperty = "4" };
            var f = new ComplexObject { Id = 5, Second = 5, MyProperty = "5" };


            BST<ComplexObject> tree = new BST<ComplexObject>(StaticFunctions.DefaultFunction)
            {
                { 1, a },
                { 2, b },
                { 3, c },
                { 4, d },
                { 5, e },
                { 6, f }
            };


            bool result = true;

            var itemChanged = tree.Find(3);

            tree.Root.Right.Get.SetKey = 11;


            result = tree.Root.Right.Right.Right.Right.Right.Get.Key == 11;

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestComplex()
        {
            var a = new ComplexObject { Id = 0, Second = 0, MyProperty = "0" };
            var b = new ComplexObject { Id = 1, Second = 1, MyProperty = "1" };
            var c = new ComplexObject { Id = 2, Second = 2, MyProperty = "2" };
            var d = new ComplexObject { Id = 3, Second = 3, MyProperty = "3" };
            var e = new ComplexObject { Id = 4, Second = 4, MyProperty = "4" };
            var f = new ComplexObject { Id = 5, Second = 5, MyProperty = "5" };


            BST<ComplexObject> tree = new BST<ComplexObject>(StaticFunctions.DefaultFunction)
            {
                { 1, a },
                { 2, b },
                { 3, c },
                { 4, d },
                { 5, e },
                { 6, f }
            };


            bool result = true;

            var itemChanged = tree.Find(3);

            tree.Root.Right.Get.SetKey = 11;

            result = tree.Root.Right.Right.Right.Right.Right.Get.Key == 11;

            Assert.IsTrue(result);
        }
    }
}
