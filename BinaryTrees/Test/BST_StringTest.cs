﻿namespace Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using BinatryTrees.Samples.BSTs;

    [TestClass]
    public class BST_StringTest
    {
        public string TestHash(object Object)
        {
            return (string)(Object);
        }

        [TestMethod]
        public void Add()
        {
            BHT_String<string> tree = new BHT_String<string>(TestHash)
            {
                "d",
                "c",
                "a",
                "b",
                "g",
                "f",
                "e",
                "h"
            };
            var a = tree.Root.Get.Value == "d";
            var b = tree.Root.Right.Get.Value == "g";
            var c = tree.Root.Right.Right.Get.Value == "h";
            var d = tree.Root.Left.Get.Value == "c";
            var e = tree.Root.Left.Left.Get.Value == "a";
            var f = tree.Root.Left.Left.Right.Get.Value == "b";
            var g = tree.Root.Right.Left.Left.Get.Value == "e";

            Assert.IsTrue(a && b && c && d && e && f && g);
        }

        [TestMethod]
        public void Has()
        {
            BHT_String<string> tree = new BHT_String<string>(TestHash)
            {
                "5",
                "15",
                "25",
                "13",
                "2",
                "19",
                "23",
                "1"
            };
            bool result = tree.Contains("5") && tree.Contains("2") && tree.Contains("23");

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Search()
        {
            BHT_String<string> tree = new BHT_String<string>(TestHash)
            {
                "5",
                "15",
                "25",
                "13",
                "2",
                "19",
                "23",
                "1"
            };
            var _5 = tree.Find("5");
            var _2 = tree.Find("2");
            var _23 = tree.Find("23");
            var _45 = tree.Find("45");


            bool result = _5 == "5" && _2 == "2" && _23 == "23" && _45 == null;

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RemoveRoot()
        {
            BHT_String<string> tree = new BHT_String<string>(TestHash)
            {
                "5"
            };
            tree.Remove("5");

            var result = tree.Contains("5");

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void RemoveChildWithLeftNull()
        {
            BHT_String<string> tree = new BHT_String<string>(TestHash)
            {
                "f",
                "b",
                "d",
                "c",
                "e"
            };
            tree.Remove("b");

            var d = tree.Contains("f");
            var a = !tree.Contains("b");
            var b = tree.Contains("d");
            var c = tree.Contains("c");
            var e = tree.Contains("e");


            var result = a && b && c && d && e;

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RemoveChildWithRightNull()
        {
            BHT_String<string> tree = new BHT_String<string>(TestHash)
            {
                "5",
                "4",
                "3",
                "1"
            };
            tree.Remove("4");

            var result = !tree.Contains("4") && tree.Contains("3") && tree.Contains("1");

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RemoveLeft()
        {
            BHT_String<string> tree = new BHT_String<string>(TestHash)
            {
                "5",
                "4",
                "3",
                "2",
                "1"
            };
            tree.Remove("4");

            var a = !tree.Contains("4");
            var b = tree.Contains("3");
            var c = tree.Contains("2");
            var d = tree.Contains("1");

            var result = a && b && c && d;

            Assert.IsTrue(result);
        }


        [TestMethod]
        public void RemoveComplex()
        {
            BHT_String<string> tree = new BHT_String<string>(TestHash)
            {
                "8",
                "4",
                "2",
                "6",
                "3",
                "5",
                "7",
                "12",
                "10",
                "14",
                "11",
                "13",
                "16",
                "1"
            };
            tree.Remove("12");

            var result = !tree.Contains("12") && tree.Contains("3") && tree.Contains("2") && tree.Contains("11") &&
                         !tree.Contains("9") && tree.Contains("13") && tree.Contains("16");

            Assert.IsTrue(result);
        }
    }
}
