﻿namespace BinaryTrees.BSTs
{
    using BinaryTrees.Parts.Delegates;
    using BinaryTrees.Parts.Nodes;
    using BinaryTrees.Parts.Pairs;

    public abstract class BinaryHashTree<KeyType, ValueType> : BinarySearchTree<KeyType, ValueType>
    {
        public new Node<KeyType, ValueType> First { get; protected set; }

        /// <summary>
        /// Set your own hash method
        /// </summary>
        /// <param name="hashFunction">YourHasFunction(object) returns int</param>
        public BinaryHashTree(HashFunction<KeyType> hashFunction) : base()
        {
            HashDelegate = hashFunction;
        }

        public virtual void Add(ValueType value)
        {
            HashValuePair<KeyType, ValueType> pairValue = new HashValuePair<KeyType, ValueType>(value, HashDelegate);
            HashNode<KeyType, ValueType> node = new HashNode<KeyType, ValueType>(pairValue);
            pairValue.PropertyChanged += OnPropertyChanged;

            if (Root != null)
            {
                AddToTree(node, Root);
            }
            else
            {
                
                Root = node;
                First = node;
            }

            Count++;
        }
    }
}
