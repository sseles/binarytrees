﻿namespace BinaryTrees.BSTs
{
    using BinaryTrees.Parts.Delegates;
    using BinaryTrees.Parts.Nodes;
    using BinaryTrees.Parts.Pairs;
    using System;
    using System.Collections;
    /// <summary>
    /// KeyType is used for search and comparison, Im stands for Immutable. If you change a part of object wich uses hash this tree will be unsearchable.
    /// </summary>
    public abstract class BinarySearchTree<KeyType, ValueType> : BinaryTree<KeyType, ValueType>, IEnumerable
    {
        #region Properties
        public int Count { get; protected set; }
        public Node<KeyType, ValueType> First { get; protected set; }
        protected HashFunction<KeyType> HashDelegate { get; set; }
        #endregion


        #region Constructors
        public BinarySearchTree() : base()
        {

        }

        #endregion
        #region Public
        public virtual void Add(KeyType key, ValueType value)
        {
            KeyValuePair<KeyType, ValueType> pairValue = new KeyValuePair<KeyType, ValueType>(key, value);
            Node<KeyType, ValueType> node = new Node<KeyType, ValueType>(pairValue);
            pairValue.PropertyChanged += OnPropertyChanged;

            if (Root != null)
            {
                AddToTree(node, Root);
            }
            else
            {
                
                Root = node;
                First = node;
            }

            Count++;
        }

        /// <summary>
        /// Find a object with coresponding key
        /// </summary>
        /// <returns>True if key is in the tree</returns>
        public bool Contains(KeyType key)
        {
            return ContainsKey(key, Root);
        }

        /// <summary>
        /// Get object with coresponding key
        /// </summary>
        public ValueType Find(KeyType key)
        {
            return FindKey(key, Root);
        }

        /// <summary>
        /// Remove item with the given key
        /// </summary>
        /// <returns>True if item was found and deleted</returns>
        public bool Remove(KeyType key)
        {
            return RemoveItem(key, Root);
        }

        public IEnumerator GetEnumerator()
        {
            Node<KeyType, ValueType> current = First;
            for (int index = 0; index < Count; index++)
            {
                if (index != 0)
                {
                    current = Next(current);
                }
                yield return current.Get.Value;
            }
        }
        #endregion
        #region Protected Binary Tree Functions
        /// <summary>
        /// Check the node and set value accordingly ( recursive function )
        /// </summary>
        /// <param name="value">Value to check</param>
        /// <param name="node">Node to search</param>
        protected void AddToTree(Node<KeyType, ValueType> value, Node<KeyType, ValueType> node)
        {
            int position = Comparer(value.Get.Key, node.Get.Key);

            if (position >= 0)
            {
                if (node.Right != null)
                {
                    AddToTree(value, node.Right);
                }
                else
                {
                    node.SetRightChild = value;
                    node.Right.SetParent = node;
                }
            }
            else
            {
                if (node.Left != null)
                {
                    AddToTree(value, node.Left);
                }
                else
                {
                    position = Comparer(value.Get.Key, First.Get.Key);
                    node.SetLeftChild = value;
                    node.Left.SetParent = node;
                    if (position < 0)
                    {
                        First = node.Left;
                    }
                }
            }
        }

        protected bool ContainsKey(KeyType key, Node<KeyType, ValueType> node)
        {
            if (node == null) return false;
            if (node.Get == null) return false;

            int position = Comparer(key, node.Get.Key);

            if (position > 0)
            {
                if (node.Right != null)
                {
                    return ContainsKey(key, node.Right);
                }
            }
            else if (position < 0)
            {
                if (node.Left != null)
                {
                    return ContainsKey(key, node.Left);
                }
            }
            else
            {
                return true;
            }

            return false;
        }

        protected ValueType FindKey(KeyType key, Node<KeyType, ValueType> node)
        {
            int position = Comparer(key, node.Get.Key);

            if (position > 0)
            {
                if (node.Right != null)
                {
                    return FindKey(key, node.Right);
                }
            }
            else if (position < 0)
            {
                if (node.Left != null)
                {
                    return FindKey(key, node.Left);
                }
            }
            else
            {
                return node.Get.Value;
            }

            return default(ValueType);
        }

        protected bool RemoveItem(KeyType key, Node<KeyType, ValueType> node)
        {
            if (Root == null) return false;

            int position = Comparer(key, node.Get.Key);

            if (position > 0)
            {
                if (node.Right != null)
                {
                    return RemoveItem(key, node.Right);
                }
            }
            else if (position < 0)
            {
                if (node.Left != null)
                {
                    return RemoveItem(key, node.Left);
                }
            }
            else
            {
                DeleteNode(node);
                Count--;
                return true;
            }

            return false;
        }

        protected void DeleteNode(Node<KeyType, ValueType> current)
        {
            Node<KeyType, ValueType> parent = current.Parent;

            if (current.Right == null)
            {
                DeleteRightNull(current, parent);
            }
            else if (current.Right.Left == null)
            {
                DeleteRightLeftNull(current, parent);
            }
            else
            {
                DeleteWithoutNull(current, parent);
            }
        }

        protected void DeleteRightNull(Node<KeyType, ValueType> current, Node<KeyType, ValueType> parent)
        {
            if (parent == null)
            {
                SetParent(current.Left, Root);
                Root = current.Left;
            }
            else
            {
                int result = Comparer(parent.Get.Key, current.Get.Key);
                if (result > 0)
                {
                    parent.SetLeftChild = current.Left;
                    SetParent(parent.Left, parent);
                }
                else if (result < 0)
                {
                    parent.SetRightChild = current.Left;
                    SetParent(parent.Right, parent);
                }
            }
        }

        protected void DeleteRightLeftNull(Node<KeyType, ValueType> current, Node<KeyType, ValueType> parent)
        {
            current.Right.SetLeftChild = current.Left;

            if (parent == null)
            {
                SetParent(current.Right, Root);
                Root = current.Right;
            }
            else
            {
                int result = Comparer(parent.Get.Key, current.Get.Key);
                if (result > 0)
                {
                    parent.SetLeftChild = current.Right;
                    SetParent(parent.Left, parent);
                }
                else if (result < 0)
                {
                    parent.SetRightChild = current.Right;
                    SetParent(parent.Right, parent);
                }
            }
        }

        protected void DeleteWithoutNull(Node<KeyType, ValueType> current, Node<KeyType, ValueType> parent)
        {
            // We first need to find the right node's left-most child
            Node<KeyType, ValueType> leftmost = current.Right.Left, lmParent = current.Right;
            while (leftmost.Left != null)
            {
                lmParent = leftmost;
                leftmost = leftmost.Left;
            }

            // the parent's left subtree becomes the leftmost's right subtree

            lmParent.SetLeftChild = leftmost.Right;
            SetParent(lmParent.Left, lmParent);

            leftmost.SetLeftChild = current.Left;
            SetParent(leftmost.Left, leftmost);
            leftmost.SetRightChild = current.Right;
            SetParent(leftmost.Right, leftmost);

            if (parent == null)
            {
                Root = leftmost;
                Root.SetParent = null;
            }
            else
            {
                int result = Comparer(parent.Get.Key, current.Get.Key);
                if (result > 0)
                {
                    parent.SetLeftChild = leftmost;
                    SetParent(parent.Left, parent);
                }
                else if (result < 0)
                {
                    parent.SetRightChild = leftmost;
                    SetParent(parent.Right, parent);
                }
            }
        }

        protected void SetParent(Node<KeyType, ValueType> node, Node<KeyType, ValueType> parrent)
        {
            if (parrent != null)
                if (node != null)
                    node.SetParent = parrent;
        }

        protected Node<KeyType, ValueType> Next(Node<KeyType, ValueType> current)
        {
            if (current.Right != null)
            {
                return IterateLeft(current.Right);
            }
            else
            {
                return IterateNext(current, current);
            }
        }

        protected Node<KeyType, ValueType> IterateLeft(Node<KeyType, ValueType> node)
        {
            if (node.Left != null)
            {
                return IterateLeft(node.Left);
            }
            else
            {
                return node;
            }
        }

        protected Node<KeyType, ValueType> IterateUp(Node<KeyType, ValueType> node)
        {
            if (node.Parent != null)
            {
                return node.Parent;
            }
            else
            {
                return node.Right;
            }
        }

        protected Node<KeyType, ValueType> IterateNext(Node<KeyType, ValueType> node, Node<KeyType, ValueType> parent)
        {
            var position = Comparer(node.Get.Key, parent.Get.Key);

            if (position >= 0)
            {
                parent = IterateUp(parent);
                return IterateNext(node, parent);
            }
            else
            {
                return parent;
            }
        }
        #endregion
        #region Protected Event Functions
        protected void OnPropertyChanged(object sender, EventArgs e)
        {
            KeyValuePair<KeyType, ValueType> objectSender = (KeyValuePair<KeyType, ValueType>)sender;

            Remove(objectSender.Key);
            Add(objectSender.Key, objectSender.Value);
        }
        #endregion
        #region Abstract
        /// <summary>
        /// Overade this method with method that will compare your 2 key values ( can return any number in int not necery to be -1, 0 or 1 // key < nodeKey)
        /// </summary>
        /// <returns>-1 if the value is lower, 0 if they are equal and 1 if value is higher</returns>
        protected abstract int Comparer(KeyType key, KeyType nodeKey);
        #endregion
    }
}
