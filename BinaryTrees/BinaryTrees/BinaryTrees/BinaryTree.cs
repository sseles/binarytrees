﻿namespace BinaryTrees
{
    using BinaryTrees.Parts.Nodes;

    public class BinaryTree<KeyType,ValueType>
    {
        public Node<KeyType, ValueType> Root { get; set; }
        
        public BinaryTree()
        {
            Root = null;
        }

        public virtual void Clear()
        {
            Root = null;
        }
    }
}
