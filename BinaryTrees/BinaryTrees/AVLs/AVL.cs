﻿namespace BinaryTrees.AVLs
{
    using BinaryTrees.BSTs;
    using BinaryTrees.Parts.Delegates;

    public abstract class AVL<KeyType,ValueType> : BinarySearchTree<KeyType,ValueType>
    {
        public int TreeHight { get; protected set; }
        public int LeftNodeHight { get; protected set; }
        public int RightNodeHight { get; protected set; }

        public AVL() : base()
        {

        }
    }
}
