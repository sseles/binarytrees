﻿namespace BinaryTrees.Parts.Nodes
{
    using BinaryTrees.Parts.Pairs;

    public class HashNode<KeyType, ValueType> : Node<KeyType, ValueType>
    {
        public new HashNode<KeyType, ValueType> Parent
        {
            get
            {
                return (HashNode<KeyType, ValueType>)base.Parent;
            }
        }
        public new HashNode<KeyType, ValueType> Left
        {
            get
            {
                return (HashNode<KeyType, ValueType>)base.Left;
            }
        }
        public new HashNode<KeyType, ValueType> Right
        {
            get
            {
                return (HashNode<KeyType, ValueType>)base.Right;
            }
        }
        public new HashValuePair<KeyType, ValueType> Get
        {
            get
            {
                return (HashValuePair<KeyType, ValueType>)base.Get;
            }
        }

        public HashNode(HashValuePair<KeyType, ValueType> value) : base(value)
        {
        }

        public HashNode(HashValuePair<KeyType, ValueType> value, HashNode<KeyType, ValueType> parent) : base(value, parent)
        {
        }
    }
}
