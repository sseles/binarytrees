﻿namespace BinaryTrees.Parts.Nodes
{
    using BinaryTrees.Parts.Pairs;
    using System;

    public class Node<KeyType,ValueType> : IDisposable
    {
        public Node<KeyType, ValueType> Parent { get; protected set; }
        public Node<KeyType, ValueType> Left { get; protected set; }
        public Node<KeyType, ValueType> Right { get; protected set; }
        public BasePair<KeyType, ValueType> Get { get; protected set; }

        public Node(BasePair<KeyType, ValueType> value)
        {
            Get = value;
            Parent = null;
        }

        public Node(BasePair<KeyType, ValueType> value, Node<KeyType, ValueType> parent)
        {
            Get = value;
            Parent = parent;
        }

        public Node<KeyType, ValueType> SetParent
        {
            set
            {
                Parent = value;
            }
        }

        public Node<KeyType, ValueType> SetLeftChild
        {
            set
            {
                Left = value;
            }
        }

        public Node<KeyType, ValueType> SetRightChild
        {
            set
            {
                Right = value;
            }
        }

        public virtual void Dispose()
        {
            Parent = null;
            Left = null;
            Right = null;
            Get = null;
        }
    }
}
