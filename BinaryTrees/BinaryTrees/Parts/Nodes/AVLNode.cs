﻿namespace BinaryTrees.Parts.Nodes
{
    using BinaryTrees.Parts.Pairs;

    public class AVLNode<KeyType, ValueType> : Node<KeyType, ValueType>
    {
        public int? NodeHeight { get; protected set; }

        public AVLNode(BasePair<KeyType, ValueType> value) : base(value)
        {

        }

        public AVLNode(BasePair<KeyType, ValueType> value, AVLNode<KeyType, ValueType> parent) : base(value, parent)
        {

        }

        public int SetNodeHeight
        {
            set
            {
                NodeHeight = value;
            }
        }

        public override void Dispose()
        {
            NodeHeight = null;
            base.Dispose();
        }
    }
}
