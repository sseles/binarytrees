﻿namespace BinaryTrees.Parts.Pairs
{
    using System;
    using System.ComponentModel;

    public class BasePair<KeyType, ValueType> : INotifyPropertyChanged
    {
        public KeyType Key { get; protected set; }
        public ValueType Value { get; protected set; }

        public BasePair(ValueType value)
        {
            Value = value;
        }

        public virtual KeyType SetKey
        {
            set
            {
                Key = value;
            }
        }

        public virtual ValueType SetValue
        {
            set
            {
                Value = value;
            }
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
