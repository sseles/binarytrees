﻿namespace BinaryTrees.Parts.Pairs
{
    using BinaryTrees.Parts.Delegates;

    public class HashValuePair<KeyType, ValueType> : BasePair<KeyType, ValueType>
    {
        protected HashFunction<KeyType> HashDelegate { get; set; }
        protected new KeyType SetKey
        {
            set
            {
                base.Key = value;
            }
        }

        public HashValuePair(ValueType value, HashFunction<KeyType> HashDelegate) : base(value)
        {
            this.HashDelegate = HashDelegate;
            Key = HashDelegate(value);
        }

        public override ValueType SetValue
        {
            set
            {
                base.Value = value;
                Key = HashDelegate(value);
                OnPropertyChanged("Value");
            }
        }
    }
}
