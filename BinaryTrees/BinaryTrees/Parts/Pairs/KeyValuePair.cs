﻿using System;

namespace BinaryTrees.Parts.Pairs
{
    public class KeyValuePair<KeyType, ValueType> : BasePair<KeyType, ValueType>
    {
        public KeyValuePair(KeyType key, ValueType value) : base(value)
        {
            Key = key;
        }

        public override KeyType SetKey
        {
            set
            {
                base.SetKey = value;
                OnPropertyChanged("Key");
            }
        }
    }
}
