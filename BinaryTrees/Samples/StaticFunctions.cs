﻿namespace BinaryTrees.Samples
{
    using System;
    public static class StaticFunctions
    {
        /// <summary>
        /// This is default microsoft .net GetHashCode function
        /// </summary>
        public static int DefaultFunction(object Object)
        {
            return Object.GetHashCode();
        }

        public static int UseIdFunction(ComplexObject Object)
        {
            return Object.Id;
        }

        public static void Nothing(int value)
        {

        }
    }
}
