﻿namespace BinaryTrees.Samples.BSTs
{
    using BinaryTrees.BSTs;
    using BinaryTrees.Parts.Delegates;

    /// <summary>
    /// This tree uses int as a Key for comparison
    /// </summary>
    public class BST<ValueType> : BinarySearchTree<int, ValueType>
    {
        public BST(HashFunction<int> function) : base()
        {

        }

        protected override int Comparer(int key, int nodeKey)
        {
            if (key < nodeKey)
            {
                return -1;
            }
            else if (key == nodeKey)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
    }
}
