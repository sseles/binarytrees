﻿namespace BinatryTrees.Samples.BSTs
{
    using BinaryTrees.BSTs;
    using BinaryTrees.Parts.Delegates;
    using System;
    /// <summary>
    /// This tree uses string as a Key for comparison
    /// </summary>
    public class BHT_String<ValueType> : BinaryHashTree<string, ValueType>
    {

        public BHT_String(HashFunction<string> function) : base(function)
        {

        }

        protected override int Comparer(string key, string nodeKey)
        {
            return String.CompareOrdinal(key, nodeKey);
        }
    }
}
