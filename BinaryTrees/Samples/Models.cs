﻿namespace BinaryTrees.Samples
{
    public class ComplexObject
    {
        public int Id { get; set; }
        public int Second { get; set; }
        public string MyProperty { get; set; }

        public override int GetHashCode()
        {
            int h = -2128831035;
            h = (h * 16777619) ^ Id;
            h = (h * 16777619) ^ Second;
            h = (h * 16777619) ^ MyProperty.GetHashCode();
            return h;
        }
    }
}
