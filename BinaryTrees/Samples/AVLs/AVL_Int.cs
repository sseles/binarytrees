﻿namespace BinaryTrees.Samples.AVLs
{
    using BinaryTrees.BSTs;
    using BinaryTrees.Parts.Delegates;
    /// <summary>
    /// This tree uses int as a Key for comparison
    /// </summary>
    public class AVL_Int<ValueType> : BinaryHashTree<int, ValueType>
    {
        public AVL_Int(HashFunction<int> function) : base(function)
        {

        }

        protected override int Comparer(int key, int nodeKey)
        {
            if (key < nodeKey)
            {
                return -1;
            }
            else if (key == nodeKey)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
    }
}
